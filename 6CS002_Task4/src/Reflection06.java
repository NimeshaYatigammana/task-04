import java.lang.reflect.Field;

public class Reflection06 {
	
	public static void main(String[] args) throws Exception {
		
	    RSample sample = new RSample();
	    
	    System.out.println("Reflection06");
	    Field[] fields = sample.getClass().getDeclaredFields();
	    
	    System.out.printf("%d fields are running...\n", fields.length);


	    for (Field f : fields) {
	      System.out.printf("field name=%s \ttype=%s \taccessible=%s\n", f.getName(),
	          f.getType(), f.isAccessible());
	    }
	  }

}
