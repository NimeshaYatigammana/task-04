package KUnitTest;

import java.util.*;

public class KUnit {
  private static List<String> test;
  private static int Testchecks = 0;
  private static int Testpassed = 0;
  private static int Testfailed = 0;

  private static void Report(String txt) {  
    if (test == null) {
    	test = new LinkedList<String>();
    }
    test.add(String.format("Test case %d: %s", Testchecks++, txt));
  }
  public static void test1(double num1, double num2) {
	    if (num1 == num2) {
	    	Report(String.format("  %.2f  ==  %.2f", num1, num2));
	    	Testpassed++;
	    } else {
	    	Report(String.format("*  %.2f  ==  %.2f", num1, num2));
	    	Testfailed++;
	    }}
  public static void test2(double num1, double num2) {
	    if (num1 != num2) {
	    	Report(String.format("  %.2f  !=  %.2f", num1, num2));
	    	Testpassed++;
	    } else {
	    	Report(String.format("* %.2f  !=  %.2f", num1, num2));
	    	Testfailed++;
	    }} 
  public static void test3(String value1, String value2) {
	  try {
	        char n1 = value1.charAt(5);
	        char n2 = value2.charAt(5);
	        if (value1.length() >= 5 && value2.length() >= 5) {
	            if (Character.isDigit(n1) || Character.isDigit(n2)) {
	            	Report(String.format("%s and %s contain letters and numbers", value1, value2));
	                Testchecks++;
	            } else if (value1.charAt(5) == n1 && value2.charAt(5) == n2) {
	            	Report(String.format("%s and %s contain the expected characters", value1, value2));
	                Testpassed++;
	            } else {
	            	Report(String.format("%s and %s do not contain the expected characters", value1, value2));
	                Testfailed++;
	            }
	        } 
	    } catch (StringIndexOutOfBoundsException e) {
	        System.out.println("Both strings must be more than 5 letters");
	    }
	}
  public static void test4(String num1, String num2) {
		try {
			char val1 = num1.charAt(4);
			char val2 = num2.charAt(4);
			if (num1.charAt(4) == val1 && num2.charAt(4) == val2) {
				Report(String.format("%s and %s more than 4 number of strings.", num1, num2));
				Testpassed++;
		    } else {
		    	Report(String.format("%s and %s more than 4 number of strings.", num1, num2));
		    	Testfailed++;
		    }
		}catch(StringIndexOutOfBoundsException e) {
			Report(String.format("Number of String must be more than 4."));
            Testfailed++;
		}
	  }
  public static void report() { 
    System.out.printf("Test passed : %d\n", Testpassed);
    System.out.printf("Test failed : %d\n", Testfailed);
    System.out.println();
    
    for (String t : test) {
      System.out.println(t);
    }}}
