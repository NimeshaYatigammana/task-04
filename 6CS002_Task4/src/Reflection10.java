import java.lang.reflect.Method;

public class Reflection10 {
	public static void main(String[] args) throws Exception {
		
	    RSample sample = new RSample();
	    
	    System.out.println("Reflection10");
	    
	    Method method = sample.getClass().getDeclaredMethod("setnum2", double.class);
	    
	    method.setAccessible(true);
	    method.invoke(sample, 66.9);
	    System.out.println(sample);
	    

	  }
}
