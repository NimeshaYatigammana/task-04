import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Reflection09 {
	public static void main(String[] args) throws Exception {
	    RSample sample = new RSample();
	    
	    System.out.println("Reflection09");
	    Method[] methods = sample.getClass().getMethods();
	    System.out.printf("%d methods are running...\n", methods.length);

	    for (Method method : methods) {
	    	
	      System.out.printf("method name=%s \ttype=%s \tparameters = ", method.getName(),
	    		  method.getReturnType());
	      
	      Class[] types = method.getParameterTypes();
	      for (Class c : types) {
	    	  
	        System.out.print(c.getName() + " ");
	      }
	      
	      System.out.println();
	    }
	  }
}
