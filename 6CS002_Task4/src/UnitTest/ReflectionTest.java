package UnitTest;
import static KUnitTest.KUnit.*;

public class ReflectionTest {

  void Reflectiontest1(){
	  
	  Reflection reflection = new Reflection(45.7, 74.2);
	  test1(reflection.getnum1(), 45.7);
	  test1(reflection.getnum2(), 74.2);
	  test2(reflection.getnum2(), 74.2);    
	  test2(reflection.getnum2(), 45.7);    
  }

	void Reflectiontest2(){
		
		Reflection reflection = new Reflection(45.7, 74.2);
		reflection.squarenum1();
	    test1(reflection.getnum1(), 45.7);
	}
	

void Reflectiontest3(){
		Reflection reflection = new Reflection(45.7, 74.2);
		  test4("2011549", "1234");
		  reflection = new Reflection(45.7, 0);
		  test4("Nimesha", "Yatigammana");
		  reflection = new Reflection(45.7, 0);
		  test4("N", "Y");
		  
	}

  public static void main(String[] args) {
	  
	  ReflectionTest test = new ReflectionTest();
	  test.Reflectiontest1();
	  test.Reflectiontest2();
	  test.Reflectiontest3();
      report();
  }
}
